<?php
$moveKey['Rock']=1;
$moveKey['Paper']=2;
$moveKey['Scissors']=3;
$moveKey['Lizard']=4;
$moveKey['Spock']=5;
$moveKey[1]='Rock';
$moveKey[2]='Paper';
$moveKey[3]='Scissors';
$moveKey[4]='Lizard';
$moveKey[5]='Spock';

function genHash() {
    $length = 6;
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $string ='';

    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters)-1)];
    }

    return $string;
}
function computerMove($total){
    //computer calculates it's move

    $move=mt_rand ( 1,$total );
    if ($move==1){
        return 'Rock';
    }
    if ($move==2){
        return 'Paper';
    }
    if ($move==3){
        return 'Scissors';
    }
    if ($move==4){
        return 'Lizard';
    }
    if ($move==5){
        return 'Spock';
    }


}
function winLogic($hand1,$hand2){

    $returnText['state']='Lose';
    $returnText['text']='You Lost!';
    if ($hand1==$hand2){
        $returnText['state']='Tie';
        $returnText['text']='You Tied!';
    }
    if ($hand1=='Rock'){
        if ($hand2=='Scissors'){
            $returnText['state']='Win';
            $returnText['text']='Rock crushes Scissors';
        }
        if ($hand2=='Lizard'){
            $returnText['state']='Win';
            $returnText['text']='Rock crushes Lizard';
        }
        if ($hand2=='Paper'){

            $returnText['text']='Paper covers Rock';
        }
        if ($hand2=='Spock'){

            $returnText['text']='Spock vaporizes Rock';
        }


    }
    if ($hand1=='Paper'){
        if ($hand2=='Rock'){
            $returnText['state']='Win';
            $returnText['text']='Paper covers Rock';
        }
        if ($hand2=='Spock'){
            $returnText['state']='Win';
            $returnText['text']='Paper disproves Spock';
        }
        if ($hand2=='Scissors'){

            $returnText['text']='Scissors cuts Paper';
        }
        if ($hand2=='Lizard'){

            $returnText['text']='Lizard eats Paper';
        }

    }
    if ($hand1=='Scissors'){
        if ($hand2=='Paper'){
            $returnText['state']='Win';
            $returnText['text']='Scissors cuts Paper';
        }
        if ($hand2=='Lizard'){
            $returnText['state']='Win';
            $returnText['text']='Scissors decapitates Lizard';
        }
        if ($hand2=='Rock'){

            $returnText['text']='Rock crushes Scissors';
        }
        if ($hand2=='Spock'){

            $returnText['text']='Spock crushes Scissors';
        }

    }

    if ($hand1=='Lizard'){
        if ($hand2=='Spock'){
            $returnText['state']='Win';
            $returnText['text']='Lizard poisons Spock';
        }
        if ($hand2=='Paper'){
            $returnText['state']='Win';
            $returnText['text']='Lizard eats Paper';
        }
        if ($hand2=='Rock'){

            $returnText['text']='Rock crushes Lizard';
        }
        if ($hand2=='Scissors'){

            $returnText['text']='Scissors decapitates Lizard';
        }

    }
    if ($hand1=='Spock'){
        if ($hand2=='Scissors'){
            $returnText['state']='Win';
            $returnText['text']='Spock crushes Scissors';
        }
        if ($hand2=='Rock'){
            $returnText['state']='Win';
            $returnText['text']='Spock vaporises rock';
        }
        if ($hand2=='Spock'){

            $returnText['text']='Paper disproves Spock';
        }
        if ($hand2=='Lizard'){

            $returnText['text']='Lizard poisons Spock';
        }

    }
    return $returnText;
}
?>